﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WeatherDAL;
using WeatherDAL.DTOs;
using WeatherDAL.Repositories;

namespace WeatherAPI.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class WeatherForecastController : ControllerBase
	{
		private static readonly string[] Summaries = new[]
		{
			"Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
		};

		IWeatherForecastRepository weatherForecastRepository;

		public WeatherForecastController(IWeatherForecastRepository weatherForecastRepository)
		{
			this.weatherForecastRepository = weatherForecastRepository;
		}

		[HttpGet]
		public IEnumerable<WeatherForecastDTO> Get()
		{
			return weatherForecastRepository.GetAll();
		}

		[HttpGet("{id:int}")]
		public ActionResult<WeatherForecastDTO> Get(int id)
		{
			WeatherForecastDTO forecast = weatherForecastRepository.Get(id);
			if (forecast == null)
			{
				return NotFound();
			}

			return forecast;
		}

		[HttpPost]
		public ActionResult<WeatherForecast> Post([FromBody] WeatherForecastDTO forecast)
		{
			if (forecast == null)
			{
				return BadRequest();
			}

			weatherForecastRepository.Create(forecast);

			return Ok(forecast);
		}

		[HttpPut("{id:int}")]
		public ActionResult<WeatherForecast> Put(int id, [FromBody] WeatherForecastDTO forecast)
		{
			if (forecast == null)
			{
				return BadRequest();
			}

			forecast.Id = id;

			if (weatherForecastRepository.Get(id) == null)
			{
				return NotFound();
			}

			weatherForecastRepository.Update(forecast);

			return Ok(forecast);
		}

		[HttpDelete("{id:int}")]
		public ActionResult<WeatherForecastDTO> Delete(int id)
		{
			WeatherForecastDTO forecast = weatherForecastRepository.Get(id);
			if (forecast == null)
			{
				return NotFound();
			}

			weatherForecastRepository.Delete(id);

			return Ok(forecast);
		}
	}
}
