import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { Weather } from '../models/Weather';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  
  constructor(private http: HttpClient) { }
  
  private weatherUrl = 'https://localhost:44375/api/weatherforecast';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  getForecasts(): Observable<Weather[]>{
    return this.http.get<Weather[]>(this.weatherUrl);
  }

  getForecast(id: number) : Observable<Weather>{
    const url = `${this.weatherUrl}/${id}`;
    return this.http.get<Weather>(url);
  }

  addForecast(forecast: Weather): Observable<Weather> {
    return this.http.post<Weather>(this.weatherUrl, forecast, this.httpOptions);
  }

  updateForecast(forecast: Weather): Observable<any>{
    return this.http.put(this.weatherUrl + `/${forecast.id}`, forecast, this.httpOptions);
  }

  deleteForecast(forecast: Weather | number) : Observable<Weather>{
    const id = typeof forecast === 'number' ? forecast : forecast.id;
    const url = `${this.weatherUrl}/${id}`;
    
    return this.http.delete<Weather>(url, this.httpOptions);
  }
}
