import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent implements OnInit {
  public counter: number;

  constructor(private router:Router, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.counter = +this.route.snapshot.paramMap.get('c');
  }

  incrementCounter(){
    this.counter++;
  }
}
