import { Weather } from './../models/Weather';
import { WeatherService } from './../services/weather.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  private dateAsc:Boolean = false;
  forecasts: Weather[];

  constructor(private weatherService:WeatherService) { }

  ngOnInit(): void {
    this.getForecasts();
  }

  getForecasts(){
    this.weatherService.getForecasts().subscribe(forecasts => this.forecasts = forecasts);
  }

  deleteForecast(forecast: Weather){
    this.forecasts = this.forecasts.filter(f => f!= forecast);
    this.weatherService.deleteForecast(forecast).subscribe();
  }

  sortByDate(){
    if(!this.dateAsc){
      this.forecasts.sort((f1, f2) => f1.date.localeCompare(f2.date));
      this.dateAsc = true;
    }
    else{
      this.forecasts.sort((f1, f2) => f2.date.localeCompare(f1.date));
      this.dateAsc = false;
    }
  }
}
