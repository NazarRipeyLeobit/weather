import { WeatherService } from './../services/weather.service';
import { Component, OnInit } from '@angular/core';
import { Weather } from '../models/Weather';
import { Location } from '@angular/common';

@Component({
  selector: 'app-weather-add',
  templateUrl: './weather-add.component.html',
  styleUrls: ['./weather-add.component.css']
})
export class WeatherAddComponent implements OnInit {

  forecast: Weather = new Weather();

  constructor(private weatherService:WeatherService, private location:Location) { }

  ngOnInit(): void {
  }

  goBack(){
    this.location.back();
  }

  add(){
    this.weatherService.addForecast(this.forecast).subscribe(() => this.goBack());
  }
}
