import { LoginComponent } from './auth/login/login.component';
import { AuthGuard } from './auth/auth.guard';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { WeatherAddComponent } from './weather-add/weather-add.component';
import { WeatherDetailComponent } from './weather-detail/weather-detail.component';
import { WeatherComponent } from './weather/weather.component';
import { CounterComponent } from './counter/counter.component';
import { HomeComponent } from './home/home.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [ 
  { path: '', component: HomeComponent, pathMatch: 'full', data: {  animation: 'home' } },
  { path: 'counter', component: CounterComponent, canActivate:[AuthGuard], data: {  animation: 'counter' }},
  { path: 'login', component: LoginComponent },
  { path: 'weather', component: WeatherComponent },
  { path: 'weather/add', component: WeatherAddComponent },
  { path: 'weather/:id', component: WeatherDetailComponent },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
