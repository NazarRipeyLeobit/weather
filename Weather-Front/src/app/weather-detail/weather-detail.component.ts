import { WeatherService } from './../services/weather.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Weather } from '../models/Weather';
import { Location } from '@angular/common';

@Component({
  selector: 'app-weather-detail',
  templateUrl: './weather-detail.component.html',
  styleUrls: ['./weather-detail.component.css']
})
export class WeatherDetailComponent implements OnInit {

  forecast: Weather;
  constructor(private route: ActivatedRoute,
    private weatherService:WeatherService, private location: Location) { }

  ngOnInit(): void {
    this.getForecast();
  }

  getForecast(){
    const id = +this.route.snapshot.paramMap.get('id');
    this.weatherService.getForecast(id).subscribe(f => this.forecast = f);
  }
  
  goBack(){
    this.location.back();
  }

  update(){
    this.weatherService.updateForecast(this.forecast).subscribe(() => this.goBack());
  }
}
