﻿using System;
using AutoMapper;

namespace WeatherDAL.Repositories
{
	public class BaseRepository : IDisposable
	{
		protected WeatherDbContext db;
		protected IMapper mapper;
		public BaseRepository(WeatherDbContext db, IMapper mapper)
		{
			this.db = db;
			this.mapper = mapper;
		}

		public void SaveChanges()
		{
			db.SaveChanges();
		}
		public void Dispose()
		{
			db.Dispose();
		}
	}
}
