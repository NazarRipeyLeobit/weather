﻿using System.Collections.Generic;
using WeatherDAL.DTOs;

namespace WeatherDAL.Repositories
{
	public interface IWeatherForecastRepository
	{
		IEnumerable<WeatherForecastDTO> GetAll();
		WeatherForecastDTO Get(int id);
		void Create(WeatherForecastDTO item);
		void Update(WeatherForecastDTO item);
		void Delete(int id);
	}
}
