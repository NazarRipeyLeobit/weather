﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using WeatherDAL.DTOs;

namespace WeatherDAL.Repositories
{
	public class WeatherForecastRepository : BaseRepository, IWeatherForecastRepository
	{
		public WeatherForecastRepository(WeatherDbContext db, IMapper mapper)
			: base(db, mapper)
		{ }

		public void Create(WeatherForecastDTO item)
		{
			WeatherForecast forecast = mapper.Map<WeatherForecast>(item);
			db.WeatherForecasts.Add(forecast);

			SaveChanges();
		}

		public void Delete(int id)
		{
			WeatherForecast forecast = db.WeatherForecasts.Find(id);
			db.WeatherForecasts.Remove(forecast);

			SaveChanges();
		}

		public WeatherForecastDTO Get(int id)
		{
			WeatherForecast forecast = db.WeatherForecasts.Find(id);
			WeatherForecastDTO forecastDTO = mapper.Map<WeatherForecastDTO>(forecast);

			return forecastDTO;
		}

		public IEnumerable<WeatherForecastDTO> GetAll()
		{
			IEnumerable<WeatherForecastDTO> forecastDTOs = mapper.Map<IEnumerable<WeatherForecastDTO>>(db.WeatherForecasts);

			return forecastDTOs;
		}

		public void Update(WeatherForecastDTO item)
		{
			WeatherForecast forecast = mapper.Map<WeatherForecast>(item);
			db.Entry(forecast).State = EntityState.Modified;

			SaveChanges();
		}
	}
}
