﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WeatherDAL.DTOs
{
	public class WeatherForecastDTO
	{
		public int Id { get; set; }
		[Required]
		public DateTime Date { get; set; }
		[Required]
		[Range(-80, 60)]
		public int TemperatureC { get; set; }
		public int TemperatureF { get; private set; }
		[Required]
		[StringLength(250, MinimumLength = 1)]
		public string Summary { get; set; }
	}
}
