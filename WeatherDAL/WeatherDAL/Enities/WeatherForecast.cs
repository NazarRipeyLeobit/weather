﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WeatherDAL
{
	public class WeatherForecast
	{
		[Key]
		public int Id { get; set; }

		public DateTime Date { get; set; }

		public int TemperatureC { get; set; }

		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public int TemperatureF { get; private set; }

		public string Summary { get; set; }
	}
}
