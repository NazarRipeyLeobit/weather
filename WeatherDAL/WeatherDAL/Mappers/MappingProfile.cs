﻿using AutoMapper;
using WeatherDAL.DTOs;

namespace WeatherDAL.Mappers
{
	public class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateMap<WeatherForecast, WeatherForecastDTO>().ReverseMap();
		}
	}
}
