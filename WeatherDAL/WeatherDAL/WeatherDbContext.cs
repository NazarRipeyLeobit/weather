﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace WeatherDAL
{
	public class WeatherDbContext: DbContext
	{
		public DbSet<WeatherForecast> WeatherForecasts { get; set; }

		public WeatherDbContext(DbContextOptions<WeatherDbContext> options)
			:base(options)
		{
			Database.EnsureCreated();
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<WeatherForecast>()
			   .Property(w => w.TemperatureF)
			   .HasComputedColumnSql("CAST (([TemperatureC] / 0.5556 + 32) AS INT)");
		}
	}
}
