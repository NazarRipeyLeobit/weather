CREATE TABLE [dbo].[WeatherForecasts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[TemperatureC] [int] NOT NULL,
	[TemperatureF]  AS (CONVERT([int],[TemperatureC]/(0.5556)+(32))),
	[Summary] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_WeatherForecasts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


